package pl.sda.store.Main.logic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.store.Main.model.Inventory;
import pl.sda.store.Main.model.Invoice;
import pl.sda.store.Main.model.ProductSale;

public class InvoiceManager {

    EntityDao entityDao = new EntityDao();
    Invoice invoice = new Invoice();

    public void add_invoice() {

        entityDao.save(invoice);
        System.out.println("Dodano invoice o id: "+ invoice.getId());

    }

    public void add_sale(long invoiceID, Double quantity, Double price) {

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;
        Session session = sessionFactory.openSession();

        try {
            transaction = session.beginTransaction();
            //1. tworzenie inventory status

            ProductSale productSale = new ProductSale(quantity, price);// DODANIE PRARAMETRÓW ProductSale;
            entityDao.save(productSale);

            Invoice invoice = session.get(Invoice.class,invoiceID);
            //2. zapisanie inventory status

            invoice.setProductSale(productSale);
            productSale.getInvoice();

            session.update(productSale);
            session.update(invoice);


            transaction.commit();

        } catch (Exception sqle) { // dzięki try - with -resources nie musimy robić "close" na sesji
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }
}
