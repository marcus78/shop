package pl.sda.store.Main.logic;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.store.Main.model.Inventory;
import pl.sda.store.Main.model.Product;

import java.util.List;
import java.util.Optional;

public class InventoryManager {

    EntityDao entityDao = new EntityDao();

    public void inventoryStatusProduct(Long productID, Double value, Double quantity) { // DODANIE PARAMETRÓW
        // public void inventoryStatusProduct(Long productID) {

        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Transaction transaction = null;
        Session session = sessionFactory.openSession();
        try {
            transaction = session.beginTransaction();
            //1. tworzenie inventory status

            Inventory inventory = new Inventory(value, quantity);  // DODANIE PRARAMETRÓW Inventory
            //2. zapisanie inventory status
            entityDao.save(inventory); // ZAPISANIE via EntityDao
            //session.save(inventory); // A NIE W TEN SPOSOB
            // 3. Pobranie produktu z identyfikatorem productId
            Product product = session.get(Product.class, productID);

            //Gdyby produktu nie udało się odlaezc
            // to wyleci exception wykona się rollback

            inventory.setProduct(product);
            product.getInventory().add(inventory);





            session.update(product);
            session.update(inventory);

            transaction.commit();

        } catch (Exception sqle) { // dzięki try - with -resources nie musimy robić "close" na sesji
            if (transaction != null) {
                transaction.rollback();
            }
        }
    }

    public void showFullListInvetory() {
        List<Inventory> listOfAllInventories = entityDao.getListOfAll(Inventory.class);
        listOfAllInventories.forEach(System.out::println);
    }

    public void addRelation(long productId, long inventoryId) {

        Optional<Product> productOptional = entityDao.getById(Product.class, productId);
        Optional<Inventory> inventoryOptional = entityDao.getById(Inventory.class, inventoryId);

        if(productOptional.isPresent() && inventoryOptional.isPresent()) {

            Product product = productOptional.get();
            Inventory inventory = inventoryOptional.get();

            product.getInventory().add(inventory);
            inventory.getProduct();

            entityDao.save(inventory);
            entityDao.save(product);
        }
        else{
            System.out.println("Błąd. Nie mogę znaleźć obiektu");
        }
    }
}