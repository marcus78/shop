package pl.sda.store.Main.logic;

import pl.sda.store.Main.model.Inventory;
import pl.sda.store.Main.model.Product;

import java.util.List;
import java.util.Optional;

public class ProductManager {

    EntityDao entityDao = new EntityDao();
    //Product product = new Product();

    public void saveProduct(String nazwa) {

        Product product = new Product(nazwa);

        entityDao.save(product);
        System.out.println("Produkt zapisany:" + product + "id: " + product.getId());
    }

    public void showAllProducts() {

        List<Product> listOfAllProducts = entityDao.getListOfAll(Product.class);
        listOfAllProducts.forEach(System.out::println);
    }

    //public void showInventoryByIdProduct() {
    public void showInventoryByIdProduct(Long numerIdProduktu) {
        Long productId = numerIdProduktu;

        Optional<Product> productOptional = entityDao.getById(Product.class, productId);

        if (productOptional.isPresent()) {
            System.out.println(productOptional.get().getInventory());
        } else {
            System.out.println("Błąd. Nie mogę znaleźć produktu o takim ID");
        }
    }
}