package pl.sda.store.Main;

import pl.sda.store.Main.logic.*;
import pl.sda.store.Main.model.Inventory;
import pl.sda.store.Main.model.Product;

import java.util.Optional;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        ProductManager productManager = new ProductManager();
        InventoryManager inventoryManager = new InventoryManager();
        InvoiceManager invoiceManager = new InvoiceManager();
        Scanner scanner = new Scanner(System.in);

        boolean working = true;
        do {
            String linia = scanner.nextLine();
            if (linia.equalsIgnoreCase("quit")) {
                working = false;
            } else {
                String[] slowa = linia.split(" ");
                String komenda = slowa[0];
                if (komenda.equalsIgnoreCase("add_product")) {

                    productManager.saveProduct(slowa[1]);
                    System.out.println("Produkt został dodany");

                } else if (komenda.equalsIgnoreCase("add_to_inventory")) {

                    inventoryManager.inventoryStatusProduct(Long.parseLong(slowa[3]), Double.parseDouble(slowa[2]), Double.parseDouble(slowa[1]));

                }else if(komenda.equalsIgnoreCase("list_products")){
                    productManager.showAllProducts();
                }
                else if(komenda.equalsIgnoreCase("list_inventory")){

                    productManager.showInventoryByIdProduct(Long.parseLong(slowa[1]));
                }
                else if (komenda.equalsIgnoreCase("list_full_inventory")){

                    inventoryManager.showFullListInvetory();
                }
                else if(komenda.equalsIgnoreCase("add_relation")){
                    inventoryManager.addRelation(Long.parseLong(slowa[1]), Long.parseLong(slowa[2]));
                }
                else if(komenda.equalsIgnoreCase("add_invoice")){
                    invoiceManager.add_invoice();
                }
                else if(komenda.equalsIgnoreCase("add_sale")){
                    invoiceManager.add_sale(Long.parseLong(slowa[1]), Double.parseDouble(slowa[2]),Double.parseDouble(slowa[3]));
                }
            }
        }
        while (working);
        HibernateUtil.getSessionFactory().close();
    }
}