package pl.sda.store.Main.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Product implements IBaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;

    public Product(String name) {
        this.name = name;
    }
    // czytamy : wiele produktów do jednego magazynu
    @ToString.Exclude
    @OneToMany(mappedBy = "product", fetch =  FetchType.EAGER)
    private Set<Inventory> inventory;
}