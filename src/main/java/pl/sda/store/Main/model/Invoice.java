package pl.sda.store.Main.model;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Invoice implements IBaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String invoicecol;

    public Invoice(String invoicecol) {
        this.invoicecol = invoicecol;
    }
    @CreationTimestamp
    private LocalDate dateArrived;
    @UpdateTimestamp
    private LocalDate updateArrived;

    @ManyToOne
    private ProductSale productSale;
}
