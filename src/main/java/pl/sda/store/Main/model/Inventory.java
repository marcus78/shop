package pl.sda.store.Main.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class Inventory implements IBaseEntity {

    private Double value;
    private Double quantity;

    public Inventory(Double value, Double quantity) {
        this.value = value;
        this.quantity = quantity;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @CreationTimestamp
    private LocalDate dateArrived;

    // czytamy : wiele products w magazynie(inventory)
    @ManyToOne
    private Product product;

//    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER)
//    private Set<RepairOrder> repairOrders;
//
//    @ManyToMany(mappedBy = "carSet")
//    private Set<Mechanic> mechanics;
// mechanis who have been repairing me
}